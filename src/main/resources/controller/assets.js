'use strict';

var core = require('/lib/core');

exports = function exports(router) {
    router.get('/assets/{asset}', getAsset);
};

var getAsset = function getAsset(req) {
    var asset = core.loadResource(resolve('/assets/' + req.pathParams.asset));
    if (!asset) {
        return { status: 404 };
    }
    return { body: asset, contentType: getContentTypeByExt(req.pathParams.asset) };
};

var extToContentTypes = {
    'css': 'text/css',
    'js': 'application/javascript'
};

var getContentTypeByExt = function getContentTypeByExt(asset) {
    var ext = asset.substr(asset.lastIndexOf('.') + 1);
    if (extToContentTypes.hasOwnProperty(ext)) {
        return extToContentTypes[ext] + ';charset=utf-8';
    }
    return 'application/octet-stream';
};
