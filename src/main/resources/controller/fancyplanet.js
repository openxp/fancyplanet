'use strict';

var core = require('/lib/core');
var viewController = require('/controller/html');

exports = function exports(router) {
    router.get('/fancyplanet', getFancyPlanet);
};

var getFancyPlanet = function getFancyPlanet() {
    var body = viewController.html("fancyplanet.html");
    return {
        body: body,
        contentType: 'text/html'
    };
};
