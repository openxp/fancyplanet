const core = require('/lib/core');
const viewController = require('/controller/html');

exports = function (router) {
    router.get('/fancyplanet', getFancyPlanet);
};

const getFancyPlanet = function () {
    const body = viewController.html("fancyplanet.html");
    return {
        body: body,
        contentType: 'text/html'
    };
};