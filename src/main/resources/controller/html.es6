const core = require('/lib/core');

exports.html = function(htmlfile){
    var resource = core.loadResource(resolve('/assets/'+htmlfile));
    if (!resource){return {status:404}}
    return core.readText(resource);
};