const core = require('/lib/core');

exports = function (router) {
    router.get('/assets/{asset}', getAsset);
};

const getAsset = function (req) {
    var asset = core.loadResource(resolve('/assets/' + (req.pathParams.asset)));
    if (!asset){return {status:404}}
    return {body: asset, contentType:getContentTypeByExt(req.pathParams.asset)};
};

const extToContentTypes = {
    'css': 'text/css',
    'js': 'application/javascript',
};

const getContentTypeByExt = function(asset) {
    const ext = asset.substr(asset.lastIndexOf('.') + 1);
    if (extToContentTypes.hasOwnProperty(ext)) {
        return extToContentTypes[ext]+';charset=utf-8';
    }
    return 'application/octet-stream';
};