const seed = 5625463739;
const gravityForce = 2;
const shipThrust = 0.00005;
const turnTorque = 0.0002;

const random = (() => {
    let s = seed;
    return () => alea(s++)();
})();

setTimeout(() => {
    const canvasId = 'js-canvas-container';
    const width = 410;
    const height = 410;
    const background = 'transparent';
    const engine = MatterEngine.create(canvasId, {width, height, background, wireframes: false});
    engine.loadScene(seed, MainSceneFactory, {canvasId, width, height});
}, 0);

const MainSceneFactory = {
    create: (seed, matter, options) => {
        [matter.world.gravity.x, matter.world.gravity.y] = [0, 0];
        const state = MatterEngine.SceneFactory.create(MainSceneFactory, matter);
        state.input = {
            up: false,
            right: false,
            down: false,
            left: false
        };
        state.options = options;
        state.compass = NavFactory.create(options.width, options.height);
        state.trailBook = new TrailBook(64, options.width, options.height);
        state.asteroidLayer = new PIXI.Container();
        state.compassLayer = new PIXI.Container();
        state.compassLayer.addChild(state.compass.graphics);
        matter.render.container.addChild(state.trailBook.sprite);
        matter.render.container.addChild(state.asteroidLayer);
        matter.render.container.addChild(state.compassLayer);
        const [centerX, centerY] = [state.options.width / 2, state.options.height / 2];
        state.planet = MainSceneFactory.createPlanet(state.asteroidLayer, centerX, centerY);
        state.ship = MainSceneFactory.createShip(state.asteroidLayer, state.planet, centerX, centerY);
        MainSceneFactory.createMatter(state);
        setupEventListeners(state.input);
        state.iteration = 0;
        return state;
    },

    update: state => {
        [state.ship, ...state.asteroids].forEach(({matterBody}, index) => {
            let acceleration = [0, 0];
            vec2.add(acceleration, acceleration, forces.gravitation(matterBody, [state.planet]));
            [matterBody.force.x, matterBody.force.y] = vec2.add([], [matterBody.force.x, matterBody.force.y], acceleration);
            const {position} = matterBody;
        });
        MainSceneFactory.handleKeys(state);
        [state.asteroidLayer.position.x, state.asteroidLayer.position.y] = [
            0 - state.ship.matterBody.position.x + state.options.width / 2,
            0 - state.ship.matterBody.position.y + state.options.height / 2
        ];
        state.compass.render(state.asteroidLayer, state.planet, state.asteroids);
        state.trailBook.render(state.iteration, [state.ship], [
            0 - state.ship.matterBody.position.x + state.options.width / 2,
            0 - state.ship.matterBody.position.y + state.options.height / 2
        ]);
        state.iteration++;
    },

    handleKeys: ({input, ship}) => {
        Matter.Body.setAngularVelocity(ship.matterBody, ship.matterBody.angularVelocity * 0.8);
        if (input.left) {
            document.getElementById('left').className = 'key left is-down';
            ship.matterBody.torque = -turnTorque;
        }
        if (input.right) {
            document.getElementById('right').className = 'key right is-down';
            ship.matterBody.torque = turnTorque;
        }
        if (input.up) {
            document.getElementById('up').className = 'key up is-down';
            const angle = (ship.matterBody.angle - Math.PI / 2) % (Math.PI * 2);
            const {force, velocity} = ship.matterBody;
            force.x += Math.cos(angle) * shipThrust;
            force.y += Math.sin(angle) * shipThrust;
        }
        if (input.down) {
            document.getElementById('down').className = 'key down is-down';
        }
    },

    createAsteroids: (container, planet, centerX, centerY) => {
        const colors = [
            0xff0000,
            0xff5500,
            0xffff00,
            0x00ff00,
            0x0000ff,
            0x4b0082,
            0x8b00ff
        ];
        const stepSize = (Math.PI * 2) / colors.length;
        return Array(...Array(colors.length)).map((_, index) => {
            const xPos = centerX + Math.cos(index * stepSize) * 380;
            const yPos = centerY + Math.sin(index * stepSize) * 380;
            const asteroid = MainSceneFactory.createAsteroid(container, xPos, yPos, colors[index], 24);
            asteroid.matterOptions.force = {};
            asteroid.matterOptions.friction = 0.9;
            asteroid.matterOptions.frictionAir = 0;
            asteroid.matterOptions.resitution = 0.1;
            asteroid.matterOptions.mass = 1;
            [asteroid.matterOptions.force.x, asteroid.matterOptions.force.y] = calculateCircularOrbitVector(asteroid, planet, 1);
            return asteroid;
        });
    },

    createShip: (container, planet, centerX, centerY) => {
        const angle = (Math.PI * 2) / 7 / 2;
        const xPos = centerX + Math.cos(angle) * 380;
        const yPos = centerY + Math.sin(angle) * 380;
        const ship = ShipFactory.create(xPos, yPos, 10, 0);
        ship.matterOptions.force = {};
        ship.matterOptions.friction = 0;
        ship.matterOptions.frictionAir = 0;
        ship.matterOptions.resitution = 1;
        ship.matterOptions.mass = 1;
        [ship.matterOptions.force.x, ship.matterOptions.force.y] = calculateCircularOrbitVector(ship, planet, 1);
        container.addChild(ship.graphics);
        return ship;
    },

    createPlanet: (container, centerX, centerY) => {
        const planet = PlanetFactory.create(centerX, centerY, 200, 0);
        container.addChild(planet.container);
        return planet;
    },

    createMatter: state => {
        const centerX = state.options.width / 2;
        const centerY = state.options.height / 2;
        state.asteroids = MainSceneFactory.createAsteroids(state.asteroidLayer, state.planet, centerX, centerY);
        state.addMatter([state.planet, state.ship, ...state.asteroids]);
    },

    createAsteroid: (container, x, y, color, radius = 6) => {
        const asteroid = AsteroidFactory.create(x, y, radius, 0, color, (Math.floor(random() * 3) + 3) * 2);
        container.addChild(asteroid.graphics);
        return asteroid;
    }
};

function calculateCircularOrbitVector (target, planet, velocityModifier = 1) {
    const angle = Math.atan2(planet.y - target.y, planet.x - target.x);
    let desired = vec2.sub([], [planet.x, planet.y], [target.x, target.y]);
    const distance = vec2.len(desired);
    const gravityStrength = (gravityForce) / (distance * distance);
    const neighborGravity = vec2.mul([], vec2.normalize([], desired), [gravityStrength, gravityStrength]);
    let velocity = (Math.sqrt(distance / vec2.len(neighborGravity)) * vec2.len(neighborGravity)) / 16; // refactor, magic number
    velocity *= velocityModifier;
    const output = vec2.mul([], [Math.sin(-angle), Math.cos(angle)], [velocity, velocity]);
    return output;
}

function generatePolygon (avgRadius, pointCount = 6, varianceFactor = 0.5) {
    const stepSize = (Math.PI * 2) / pointCount;
    const variance = avgRadius * varianceFactor;
    return Array(...Array(pointCount)).map((_, index) => ({
        x: Math.floor(Math.cos(stepSize * index) * (avgRadius + ((random() * variance) - variance / 2))),
        y: Math.floor(Math.sin(stepSize * index) * (avgRadius + ((random() * variance) - variance / 2)))
    }));
}

const forces = {
    gravitation: (target, neighbors) => {
        let vectorSum = [0, 0];
        let steeringVector = [0, 0];
        let count = 0;
        neighbors.forEach(neighbor => {
            const distance = vec2.dist([target.position.x, target.position.y], [neighbor.matterBody.position.x, neighbor.matterBody.position.y]);
            if (distance > 0) {
                vec2.add(vectorSum, vectorSum, [neighbor.matterBody.position.x, neighbor.matterBody.position.y]);
                count++;
            }
        });
        if (count) {
            const baryCenter = vec2.div([], vectorSum, [count, count]);
            let desired = vec2.sub([], baryCenter, [target.position.x, target.position.y]);
            const distance = vec2.len(desired);
            const gravityStrength = gravityForce / (distance * distance);
            const neighborGravity = vec2.mul([], vec2.normalize([], desired), [gravityStrength, gravityStrength]);
            vec2.sub(steeringVector, neighborGravity, [target.force.x, target.force.y]);
        }
        return steeringVector;
    }
};

function setupEventListeners (input) {
    window.addEventListener('keydown', handleKeyDown(input));
    window.addEventListener('keyup', handleKeyUp(input));
}

function handleKeyUp (input) {
    return ({keyCode}) => {
        document.getElementById('left').className = 'key left';
        document.getElementById('right').className = 'key right';
        document.getElementById('up').className = 'key up';
        document.getElementById('down').className = 'key down';
        switch (keyCode) {
            case 38:
                input.up = false;
                break;
            case 39:
                input.right = false;
                break;
            case 40:
                input.down = false;
                break;
            case 37:
                input.left = false;
                break;
        }
    };
}

function handleKeyDown (input) {
    return e => {
        const {keyCode} = e;
        switch (keyCode) {
            case 38:
                input.up = true;
                break;
            case 39:
                input.right = true;
                break;
            case 40:
                e.preventDefault();
                input.down = true;
                break;
            case 37:
                input.left = true;
                break;
        }
    };
}

function hideClickArea () {
    document.getElementById('js-click-area').style.display = 'none';
}

const NavFactory = {
    create: (width, height) => {
        const graphics = new PIXI.Graphics();
        const state = {
            graphics,
            width,
            height,
            x: 0,
            y: 0,
            render: (parentContainer, planet, asteroids) => {
                return NavFactory.render(state, parentContainer, planet, asteroids);
            }
        };
        return state;
    },

    render: (state, parentContainer, planet, asteroids) => {
        const [centerX, centerY] = [state.width / 2, state.height / 2];
        const {graphics} = state;
        graphics.clear();
        [planet, ...asteroids].forEach(trackable => {
            const {position} = trackable.matterBody;
            const [vX, vY] = vec2.sub([], [position.x + parentContainer.position.x, position.y + parentContainer.position.y], [centerX, centerY]);
            if (Math.abs(vX) - trackable.size < centerX && Math.abs(vY) - trackable.size < centerY) {
                return;
            }
            const angle = Math.atan2(vY, vX);
            let xPos = centerX + Math.cos(angle) * 300;
            let yPos = centerY + Math.sin(angle) * 300;
            xPos = Math.min(state.width, Math.max(0, xPos));
            yPos = Math.min(state.height, Math.max(0, yPos));
            graphics.beginFill(trackable.color);
            graphics.drawRect(xPos - 5, yPos - 5, 10, 10);
            graphics.endFill();
        });
    }
};

const PlanetFactory = {
    create: (x, y, size, rotation) => {
        const container = new PIXI.Container();
        const atmosphere = new Atmosphere(-size * 2, -size * 2, size, '#9ad2fc', size * 4, size * 4);
        atmosphere.cache();
        container.addChild(atmosphere.sprite);
        const planet = new Planet(-size * 2, -size * 2, size, ['#1197c0', '#4bdf6b'], random() * Math.PI * 2, size * 4, size * 4); // ocean, land
        planet.cache();
        container.addChild(planet.sprite);
        const state = {
            color: 0xdedede,
            planet,
            atmosphere,
            container,
            rotation,
            size,
            x,
            y,
            update: (options) => {
                return PlanetFactory.update(state, options);
            },
            vertices: generatePolygon(size, 20, 0),
            matterOptions: {
                friction: 0.9,
                resitution: 0.01,
                isStatic: true,
                mass: 999999
            }
        };
        return state;
    },

    update: (state) => {
        const {container, matterBody, vertices} = state;
        const {x, y} = matterBody.position;
        [container.position.x, container.position.y] = [x, y];
        container.rotation = (container.rotation + 0.001) % (Math.PI * 2);
        container.cacheAsBitmap = true;
    }
};

const AsteroidFactory = {
    create: (x, y, size, rotation, color, vertexCount, varianceFactor = 0.5) => {
        const graphics = new PIXI.Graphics();
        const state = {
            color,
            graphics,
            rotation,
            size,
            x,
            y,
            update: (options) => {
                return AsteroidFactory.update(state, options);
            },
            vertices: generatePolygon(size, vertexCount, varianceFactor),
            matterOptions: {
                friction: 0,
                resitution: 0.01
            }
        };
        return state;
    },

    update: state => {
        const {graphics, matterBody} = state;
        const {vertices} = matterBody;
        const {x, y} = matterBody.position;
        Matter.Body.setAngle(matterBody, graphics.rotation);
        Matter.Vertices.rotate(vertices, matterBody.angle, matterBody.position);
        [graphics.position.x, graphics.position.y] = [x, y];
        graphics.clear();
        graphics.beginFill(state.color);
        graphics.lineStyle(2, 0xdedede);
        graphics.moveTo(vertices[0].x - x, vertices[0].y - y);
        for (let index = 1; index < vertices.length; index++) {
            graphics.lineTo(vertices[index].x - x, vertices[index].y - y);
        }
        graphics.lineTo(vertices[0].x - x, vertices[0].y - y);
        graphics.endFill();
        graphics.cacheAsBitmap = true;
    }
};

const ShipFactory = {
    create: (x, y, size, rotation) => {
        const graphics = new PIXI.Graphics();
        const state = {
            graphics,
            rotation,
            size,
            x,
            y,
            update: options => {
                return ShipFactory.update(state, options);
            },
            vertices: ShipFactory.generateVertices(size),
            matterOptions: {
                friction: 0.8,
                airFriction: 0,
                resitution: 0.1
            }
        };
        return state;
    },

    generateVertices: size => {
        const offset = size / 2;
        return [
            {x: size / 2 - offset, y: -offset * 1.7},
            {x: size - offset, y: size - offset},
            {x: 0 - offset, y: size - offset}
        ];
    },

    update: (state) => {
        const {graphics, matterBody, vertices} = state;
        const {x, y} = matterBody.position;
        graphics.rotation = matterBody.angle;
        [graphics.position.x, graphics.position.y] = [x, y];
        graphics.clear();
        graphics.beginFill(0x333333);
        graphics.lineStyle(2, 0xdedede);
        graphics.moveTo(vertices[0].x, vertices[0].y);
        for (let index = 1; index < vertices.length; index++) {
            graphics.lineTo(vertices[index].x, vertices[index].y);
        }
        graphics.lineTo(vertices[0].x, vertices[0].y);
        graphics.endFill();
        graphics.cacheAsBitmap = true;
    }
};

const emojiLookup = (() => {
    const size = 16;
    const surfaceArr = '🌵 🎪 🌴 🏠 🏥 🏣 🏪 🏫 💒 🚉 🚃'.split(' ');
    return surfaceArr.map(emoji => {
        const canvasEl = document.createElement('canvas');
        canvasEl.width = size + size / 2;
        canvasEl.height = size + size / 2;
        const ctx = canvasEl.getContext('2d');
        ctx.font = `16px AppleColorEmoji`;
        ctx.fillStyle = 'white';
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillText(emoji, canvasEl.width / 2, canvasEl.height / 2);
        return canvasEl;
    });
})();

class Atmosphere {

    constructor (x, y, radius, color, width, height) {
        this.radius = radius;
        this.color = color;
        const size = (this.radius * 2) * 2;
        const cacheEl = document.createElement('canvas');
        cacheEl.width = width;
        cacheEl.height = height;
        this.cacheCtx = cacheEl.getContext('2d');
        this.texture = PIXI.Texture.fromCanvas(cacheEl);
        this.sprite = new PIXI.Sprite(this.texture);
        this.sprite.position.x = x;
        this.sprite.position.y = y;
    }

    cache () {
        const ctx = this.cacheCtx;
        const color = chroma(this.color);
        const atmosphereSize = 2;
        const atmosphereRadius = this.radius * atmosphereSize;
        const halfSize = atmosphereRadius / 2;
        const x = atmosphereRadius;
        const y = atmosphereRadius;
        ctx.beginPath();
        const baseSize = 0.99 / atmosphereSize;
        const gradient = ctx.createRadialGradient(x, y, 0, x, y, atmosphereRadius);
        gradient.addColorStop(baseSize, `rgba(${color.alpha(0.4).rgba()})`);
        gradient.addColorStop(baseSize + (baseSize * 0.06), `rgba(${color.alpha(0.125).rgba()})`);
        gradient.addColorStop(baseSize + (baseSize * 0.18), `rgba(${color.alpha(0.118).rgba()})`);
        gradient.addColorStop(baseSize + (baseSize * 0.23), `rgba(${color.alpha(0.118).rgba()})`);
        gradient.addColorStop(baseSize + (baseSize * 0.28), `rgba(${color.alpha(0.108).rgba()})`);
        gradient.addColorStop(baseSize + (baseSize * 0.3), `rgba(${color.alpha(0.116).rgba()})`);
        gradient.addColorStop(baseSize + (baseSize * 0.5), `rgba(${color.alpha(0.07).rgba()})`);
        gradient.addColorStop(baseSize + (baseSize * 0.7), `rgba(${color.alpha(0.04).rgba()})`);
        gradient.addColorStop(1, `rgba(${color.alpha(0).rgba()})`);
        ctx.fillStyle = gradient;
        ctx.arc(x, y, atmosphereRadius, 0, Math.PI * 2); // refactor, bug, this should be 0, 0
        ctx.fill();
    }
}

class Planet {

    constructor (x, y, radius, colors, rotation, width, height) {
        this.x = x;
        this.y = y;
        this.rotation = rotation
        this.radius = radius;
        this.colors = colors;
        const size = (this.radius * 2) * 2;
        const cacheEl = document.createElement('canvas');
        cacheEl.width = width;
        cacheEl.height = height;
        this.cacheCtx = cacheEl.getContext('2d');
        this.texture = PIXI.Texture.fromCanvas(cacheEl);
        this.sprite = new PIXI.Sprite(this.texture);
        this.sprite.position.x = x;
        this.sprite.position.y = y;
    }

    generatePlanetData (lotCount) {
        const data = [];
        for (let lotIndex = 0; lotIndex < lotCount; lotIndex++) {
            data.push(1);
        }
        let backgroundForestCount = Math.floor(random() * 2) + 1;
        while (backgroundForestCount--) {
            let backgroundForestSize = Math.floor(random() * (lotCount / 8)) + 2;
            let backgroundForestIndex = Math.floor(random() * lotCount);
            while (backgroundForestSize--) {
                data[backgroundForestIndex] = 2;
                backgroundForestIndex++;
                backgroundForestIndex = backgroundForestIndex % lotCount;
            }
        }
        let foregroundForestCount = Math.floor(random() * 2) + 1;
        while (foregroundForestCount--) {
            let foregroundForestSize = Math.floor(random() * (lotCount / 8)) + 2;
            let foregroundForestIndex = Math.floor(random() * lotCount);
            while (foregroundForestSize--) {
                data[foregroundForestIndex] = 3;
                foregroundForestIndex++;
                foregroundForestIndex = foregroundForestIndex % lotCount;
            }
        }
        let oceanCount = Math.floor(random() * 2) + 1;
        while (oceanCount--) {
            let oceanSize = Math.floor(random() * (lotCount / 6)) + 3;
            let oceanIndex = Math.floor(random() * lotCount);
            while (oceanSize--) {
                data[oceanIndex] = 0;
                oceanIndex++;
                oceanIndex = oceanIndex % lotCount;
            }
        }
        return data;
    }

    drawOcean (ctx, x, y, radius, color, data, mantleSize, oceanSize) {
        const oceanRadius = radius * oceanSize;
        ctx.beginPath();
        const gradient = ctx.createRadialGradient(x, y, 0, x, y, oceanRadius);
        gradient.addColorStop(1 - (oceanSize - mantleSize), chroma(color).darken().hex());
        gradient.addColorStop(1, color);
        ctx.fillStyle = gradient;
        ctx.arc(x, y, oceanRadius, 0, Math.PI * 2);
        ctx.fill();
    }

    drawForests (ctx, x, y, radius, color, data, coreSize, mantleSize, continentSize, oceanSize) {
        const size = 16;
        const forestRadius = continentSize * radius + size * 1.8;
        let currentAngle = 0;
        const tau = Math.PI * 2;
        const stepAngle = tau / data.length;
        data.forEach((lotType, dataIndex) => {
            if (!(lotType === 2 || lotType === 3)) {
                return;
            }
            const emojiEl = emojiLookup[Math.floor(emojiLookup.length * random())];
            const angle = dataIndex * stepAngle + stepAngle / 2;
            const textX = x + Math.cos(angle) * forestRadius;
            const textY = y + Math.sin(angle) * forestRadius;
            const {width, height} = emojiEl;
            const textAngle = ((angle + Math.PI / 2) + Math.PI * 2) % (Math.PI * 2);
            ctx.translate(textX, textY);
            ctx.rotate(textAngle);
            ctx.drawImage(emojiEl, 0, 0, width, height, -size / 2, -size / 2, width, height);
            ctx.rotate(-textAngle);
            ctx.translate(-textX, -textY);
        });
    }


    drawContinents (ctx, x, y, radius, color, data, coreSize, mantleSize, continentSize, oceanSize) {
        const continentRadius = continentSize * radius;
        let currentAngle = 0;
        const tau = Math.PI * 2;
        const stepAngle = tau / data.length;
        const lineWidth = (((oceanSize * radius) - (continentSize * radius)) * 2) * 1.25;
        data.forEach((lotType, dataIndex) => {
            if (lotType === 0) {
                return;
            }
            ctx.beginPath();
            ctx.lineCap = 'round';
            ctx.strokeStyle = color;
            ctx.lineWidth = lineWidth;
            ctx.arc(x, y, continentRadius, dataIndex * stepAngle, dataIndex * stepAngle + stepAngle);
            ctx.stroke();
        });
    }

    drawCore (ctx, x, y, radius, color, coreSize) {
        const coreRadius = radius * coreSize;
        ctx.beginPath();
        const gradient = ctx.createRadialGradient(x, y, 0, x, y, coreRadius);
        gradient.addColorStop(0, color);
        gradient.addColorStop(1, chroma(color).darken().hex());
        ctx.fillStyle = gradient;
        ctx.arc(x, y, coreRadius, 0, Math.PI * 2);
        ctx.fill();
    }

    drawMantle (ctx, x, y, radius, color, coreSize, mantleSize) {
        const mantleRadius = radius * mantleSize;
        ctx.beginPath();
        const gradient = ctx.createRadialGradient(x, y, 0, x, y, mantleRadius);
        gradient.addColorStop(1 - (mantleSize - coreSize), chroma(color).darken().hex());
        gradient.addColorStop(1, color);
        ctx.fillStyle = gradient;
        ctx.arc(x, y, mantleRadius, 0, Math.PI * 2);
        ctx.fill();
    }

    cache () {
        const ctx = this.cacheCtx;
        const lotCount = 30;
        const coreSize = 0.8;
        const mantleSize = coreSize + (coreSize * 0.125)
        const oceanSize = mantleSize + (mantleSize * 0.075)
        const continentSize = mantleSize;
        const data = this.generatePlanetData(lotCount);
        const [oceanColor, landColor] = this.colors;
        const coreColor = chroma(landColor).darken(0.75).hex();
        const x = (this.radius * continentSize) * 2 + this.radius * (1 - coreSize); // refactor, not sure this is correct
        const y = (this.radius * continentSize) * 2 + this.radius * (1 - coreSize);
        this.drawOcean(ctx, x, y, this.radius, oceanColor, data, mantleSize, oceanSize); // refactor, bug, this should be 0, 0
        this.drawForests(ctx, x, y, this.radius, landColor, data, coreSize, mantleSize, continentSize, oceanSize);
        this.drawContinents(ctx, x, y, this.radius, landColor, data, coreSize, mantleSize, continentSize, oceanSize);
        this.drawMantle(ctx, x, y, this.radius, landColor, coreSize, mantleSize);
        this.drawCore(ctx, x, y, this.radius, coreColor, coreSize);
    }
}

class TrailBook {
    constructor (layerCount, width, height) {
        this.width = width;
        this.height = height;
        this.layers = Array(...Array(layerCount)).map((_, index) => {
            const layerEl = document.createElement('canvas'); // it's very slow to `drawImage` when the canvases are different sizes
            [layerEl.width, layerEl.height] = [width, height]; // this takes more space but it runs faster by using many off-screen canvases
            return {
                ctx: layerEl.getContext('2d'),
                canvas: layerEl,
                x: width/ 2,
                y: height / 2
            };
        });
        const trailEl = document.createElement('canvas');
        [trailEl.width, trailEl.height] = [width, height];
        this.trailCtx = trailEl.getContext('2d');
        this.texture = PIXI.Texture.fromCanvas(trailEl);
        this.sprite = new PIXI.Sprite(this.texture);
    }

    render (iteration, trailables, [offsetX, offsetY]) {
        const layer = this.layers.pop();
        layer.ctx.clearRect(0, 0, this.width, this.height);
        if (iteration % Math.floor(60 / 3) === 0) {
            layer.ctx.fillStyle = 'orange';
            const trailSize = 4;
            trailables.forEach(({graphics, matterBody}) => {
                const angle = Math.atan2(matterBody.velocity.y, matterBody.velocity.x);
                const [xPos, yPos] = [matterBody.position.x + offsetX, matterBody.position.y + offsetY];
                [layer.x, layer.y] = [
                    matterBody.position.x,
                    matterBody.position.y
                ];
                layer.ctx.translate(xPos, yPos);
                layer.ctx.rotate(angle);
                layer.ctx.fillRect(-trailSize / 2, -trailSize / 2, trailSize, trailSize);
                layer.ctx.rotate(-angle);
                layer.ctx.translate(-xPos, -yPos);
            });
        }
        this.layers.unshift(layer);
        this.trailCtx.clearRect(0, 0, this.width, this.height);
        const alphaStepSize = 1 / this.layers.length;
        this.layers.forEach((layer, index) => {
            this.trailCtx.globalAlpha = ((this.layers.length - 1) - index) * alphaStepSize;
            const [xPos, yPos] = [layer.x + offsetX - this.width / 2, layer.y + offsetY - this.height / 2];
            this.trailCtx.translate(xPos, yPos);
            this.trailCtx.drawImage(layer.canvas, 0, 0, this.width, this.height, 0, 0, this.width, this.height);
            this.trailCtx.translate(-xPos, -yPos);
        });
        this.trailCtx.globalAlpha = 1;
    }
}

// fps meter
(function(){var script=document.createElement('script');script.onload=function(){var stats=new Stats();document.body.appendChild(stats.dom);requestAnimationFrame(function loop(){stats.update();requestAnimationFrame(loop)});};script.src='//rawgit.com/mrdoob/stats.js/master/build/stats.min.js';document.head.appendChild(script);})()