(function() {
    var AetherMatter, MatterEngine;

    AetherMatter = {
        defaults: {
            controller: AetherMatter,
            element: null,
            canvas: null,
            renderer: null,
            stage: null,
            spriteContainer: null,
            pixiOptions: null,
            options: {
                width: 800,
                height: 600,
                background: '#fafafa',
                wireframeBackground: '#222',
                hasBounds: false,
                enabled: true,
                wireframes: false,
                showSleeping: true,
                showDebug: false,
                showBroadphase: false,
                showBounds: false,
                showVelocity: false,
                showCollisions: false,
                showAxes: false,
                showPositions: false,
                showAngleIndicator: false,
                showIds: false,
                showShadows: false
            }
        },
        create: function(options) {
            var render, transparent;
            if (options == null) {
                options = {};
            }
            render = Matter.Common.extend({}, AetherMatter.defaults, options);
            transparent = !render.options.wireframes && render.options.background === 'transparent';
            render.pixiOptions = render.pixiOptions || {
                    view: render.canvas,
                    transparent: transparent,
                    backgroundColor: options.background,
                    antialias: true
                };
            render.renderer = render.renderer || new PIXI.autoDetectRenderer(render.options.width, render.options.height, render.pixiOptions);
            render.stage = render.stage || new PIXI.Container;
            render.spriteContainer = render.spriteContainer || new PIXI.Container;
            render.canvas = render.canvas || render.renderer.view;
            render.bounds = render.bounds || {
                    min: {
                        x: 0,
                        y: 0
                    },
                    max: {
                        x: render.options.width,
                        y: render.options.height
                    }
                };
            render.textures = {};
            render.sprites = {};
            render.primitives = {};
            render.stage.addChild(render.spriteContainer);
            render.element.appendChild(render.canvas);
            render.canvas.oncontextmenu = function() {
                return false;
            };
            render.canvas.onselectstart = function() {
                return false;
            };
            return render;
        },
        clear: function(render) {
            var bgSprite, spriteContainer, stage;
            stage = render.stage, spriteContainer = render.spriteContainer;
            stage.children.forEach(function(child) {
                return stage.removeChild(child);
            });
            spriteContainer.children.forEach(function(child) {
                return spriteContainer.removeChild(child);
            });
            bgSprite = render.sprites['bg-0'];
            render.textures = {};
            render.sprites = {};
            render.primitives = {};
            render.sprites['bg-0'] = bgSprite;
            if (bgSprite) {
                stage.addChildAt(bgSprite, 0);
            }
            render.stage.addChild(render.spriteContainer);
            render.currentBackground = null;
            stage.scale.set(1, 1);
            return stage.position.set(0, 0);
        },
        setBackground: function(render, background) {
            var bgSprite, color, isColor, texture;
            if (render.currentBackground === background) {
                return;
            }
            isColor = background.indexOf && background.indexOf('#') !== -1;
            bgSprite = render.sprites['bg-0'];
            if (isColor) {
                color = Matter.Common.colorToNumber(background);
                render.renderer.backgroundColor = color;
                if (bgSprite) {
                    render.stage.removeChild(bgSprite);
                }
            } else {
                if (!bgSprite) {
                    texture = AetherMatter._getTexture(render, background);
                    bgSprite = render.sprites['bg-0'] = new PIXI.Sprite(texture);
                    bgSprite.position.x = 0;
                    bgSprite.position.y = 0;
                    render.stage.addChildAt(bgSprite, 0);
                }
            }
            return render.currentBackground = background;
        },
        world: function(engine) {
            var allConstraints, bodies, body, bodyA, bodyB, boundsHeight, boundsScaleX, boundsScaleY, boundsWidth, constraint, constraints, i, l, len, len1, len2, len3, m, n, options, pointAWorld, pointBWorld, render, renderer, stage, world;
            render = engine.render, world = engine.world;
            renderer = render.renderer, stage = render.stage, options = render.options;
            bodies = Matter.Composite.allBodies(world);
            allConstraints = Matter.Composite.allConstraints(world);
            constraints = [];
            if (options.wireframes) {
                AetherMatter.setBackground(render, options.wireframeBackground);
            } else {
                AetherMatter.setBackground(render, options.background);
            }
            boundsWidth = render.bounds.max.x - render.bounds.min.x;
            boundsHeight = render.bounds.max.y - render.bounds.min.y;
            boundsScaleX = boundsWidth / render.options.width;
            boundsScaleY = boundsHeight / render.options.height;
            if (options.hasBounds) {
                for (i = 0, len = bodies.length; i < len; i++) {
                    body = bodies[i];
                    body.render.sprite.visible = Bounds.overlaps(body.bounds, render.bounds);
                }
                for (l = 0, len1 = constraints.length; l < len1; l++) {
                    constraint = constraints[l];
                    bodyA = constraint.bodyA;
                    bodyB = constraint.bodyB;
                    pointAWorld = constraint.pointA;
                    pointBWorld = constraint.pointB;
                    if (bodyA) {
                        pointAWorld = Vector.add(bodyA.position, constraint.pointA);
                    }
                    if (bodyB) {
                        pointBWorld = Vector.add(bodyB.position, constraint.pointB);
                    }
                    if (!pointAWorld || !pointBWorld) {
                        continue;
                    }
                    if (Bounds.contains(render.bounds, pointAWorld) || Bounds.contains(render.bounds, pointBWorld)) {
                        constraints.push(constraint);
                    }
                }
                stage.scale.set(1 / boundsScaleX, 1 / boundsScaleY);
                stage.position.set(-render.bounds.min.x * (1 / boundsScaleX), -render.bounds.min.y * (1 / boundsScaleY));
            } else {
                constraints = allConstraints;
            }
            for (m = 0, len2 = bodies.length; m < len2; m++) {
                body = bodies[m];
                AetherMatter.body(engine, body);
            }
            for (n = 0, len3 = constraints.length; n < len3; n++) {
                constraint = constraints[n];
                AetherMatter.constraint(engine, constraint);
            }
            return renderer.render(stage);
        },
        constraint: function(engine, constraint) {
            var bodyA, bodyB, constraintRender, pointA, pointB, primitive, primitiveId, render, stage;
            render = engine.render, stage = engine.stage;
            bodyA = constraint.bodyA, bodyB = constraint.bodyB, pointA = constraint.pointA, pointB = constraint.pointB;
            constraintRender = constraint.render;
            primitiveId = "c-" + constraint.id;
            primitive = render.primitives[primitiveId];
            if (!primitive) {
                primitive = render.primitives[primitiveId] = new PIXI.Graphics();
            }
            if (!constraintRender.visible || !constraint.pointA || !constraint.pointB) {
                primitive.clear();
                return;
            }
            if (Matter.Common.indexOf(stage.children, primitive) === -1) {
                stage.addChild(primitive);
            }
            primitive.clear();
            primitive.beginFill(0, 0);
            primitive.lineStyle(constraintRender.lineWidth, Matter.Common.colorToNumber(constraintRender.strokeStyle), 1);
            if (bodyA) {
                primitive.moveTo(bodyA.position.x + pointA.x, bodyA.position.y + pointA.y);
            } else {
                primitive.moveTo(pointA.x, pointA.y);
            }
            if (bodyB) {
                primitive.lineTo(bodyB.position.x + pointB.x, bodyB.position.y + pointB.y);
            } else {
                primitive.lineTo(pointB.x, pointB.y);
            }
            return primitive.endFill();
        },
        body: function(engine, body) {
            var bodyRender, primitive, primitiveId, render, sprite, spriteContainer, spriteId, stage;
            render = engine.render;
            bodyRender = body.render;
            if (!bodyRender.visible) {
                return;
            }
            if (bodyRender.sprite && bodyRender.sprite.texture) {
                spriteId = "b-" + body.id;
                sprite = render.sprites[spriteId];
                spriteContainer = render.spriteContainer;
                if (!sprite) {
                    sprite = render.sprites[spriteId] = AetherMatter._createBodySprite(render, body);
                }
                if (Matter.Common.indexOf(spriteContainer.children, sprite) === -1) {
                    spriteContainer.addChild(sprite);
                }
                sprite.position.x = body.position.x;
                sprite.position.y = body.position.y;
                sprite.rotation.y = body.angle;
                sprite.scale.x = bodyRender.sprite.xScale || 1;
                return sprite.scale.y = bodyRender.sprite.yScale || 1;
            } else {
                primitiveId = "b-" + body.id;
                primitive = render.primitives[primitiveId];
                stage = render.stage;
                if (!primitive) {
                    primitive = render.primitives[primitiveId] = AetherMatter._createBodyPrimitive(render, body);
                    primitive.initialAngle = body.angle;
                }
                if (Matter.Common.indexOf(stage.children, primitive) === -1) {
                    stage.addChild(primitive);
                }
                primitive.position.x = body.position.x;
                primitive.position.y = body.position.y;
                return primitive.rotation = body.angle - primitive.initialAngle;
            }
        },
        _createBodySprite: function(render, body) {
            var bodyRender, sprite, texture, texturePath;
            bodyRender = body.render;
            texturePath = bodyRender.sprite.texture;
            texture = AetherMatter._getTexture(render, texturePath);
            sprite = new PIXI.Sprite(texture);
            sprite.anchor.x = bodyRender.sprite.xOffset;
            sprite.anchor.y = bodyRender.sprite.yOffset;
            return sprite;
        },
        _createBodyPrimitive: function(render, body) {
            var bodyRender, fillStyle, i, j, k, l, options, part, primitive, ref, ref1, ref2, startIndex, strokeStyle, strokeStyleIndicator, strokeStyleWireframe, strokeStyleWrireframeIndicator, x, y;
            bodyRender = body.render;
            options = render.options;
            primitive = new PIXI.Graphics;
            fillStyle = Matter.Common.colorToNumber(bodyRender.fillStyle);
            strokeStyle = Matter.Common.colorToNumber(bodyRender.strokeStyle);
            strokeStyleIndicator = Matter.Common.colorToNumber(bodyRender.strokeStyle);
            strokeStyleWireframe = Matter.Common.colorToNumber('#bbb');
            strokeStyleWrireframeIndicator = Matter.Common.colorToNumber('#cd5c5c');
            primitive.clear();
            startIndex = body.parts.length > 1 ? 1 : 0;
            for (k = i = ref = startIndex, ref1 = body.parts.length; ref <= ref1 ? i < ref1 : i > ref1; k = ref <= ref1 ? ++i : --i) {
                part = body.parts[k];
                if (!options.wireframes) {
                    primitive.beginFill(fillStyle, 1);
                    primitive.lineStyle(bodyRender.lineWidth, strokeStyle, 1);
                } else {
                    primitive.beginFill(0, 0);
                    primitive.lineStyle(1, strokeStyleWireframe, 1);
                }
                primitive.moveTo(part.vertices[0].x - body.position.x, part.vertices[0].y - body.position.y);
                for (j = l = 1, ref2 = part.vertices.length; 1 <= ref2 ? l < ref2 : l > ref2; j = 1 <= ref2 ? ++l : --l) {
                    primitive.lineTo(part.vertices[j].x - body.position.x, part.vertices[j].y - body.position.y);
                }
                primitive.lineTo(part.vertices[0].x - body.position.x, part.vertices[0].y - body.position.y);
                primitive.endFill();
                if (options.showAngleIndicator || options.showAxes) {
                    primitive.beginFill(0, 0);
                    if (options.wireframes) {
                        primitive.lineStyle(1, strokeStyleWireframeIndicator, 1);
                    } else {
                        primitive.lineStyle(1, strokeStyleIndicator);
                    }
                    primitive.moveTo(part.position.x - body.position.x, part.position.y - body.position.y);
                    x = (part.vertices[0].x + part.vertices[part.vertices.length - 1].x) / 2 - body.position.x;
                    y = (part.vertices[0].y + part.vertices[part.vertices.length - 1].y) / 2 - body.position.y;
                    primitive.lineTo(x, y);
                    primitive.endFill();
                }
            }
            return primitive;
        },
        _getTexture: function(render, imagePath) {
            var texture;
            texture = render.textures[imagePath];
            if (!texture) {
                texture = render.texture[imagePath] = PIXI.Texture.fromImage(imagePath);
            }
            return texture;
        }
    };

    MatterEngine = {
        create: function(mountId, options) {
            var matter, state;
            matter = Matter.Engine.create({
                render: {
                    element: document.getElementById(mountId),
                    controller: RenderPixiCustom,
                    options: options
                }
            });
            return state = {
                matter: matter,
                loadScene: function(seed, sceneFac, options) {
                    return MatterEngine.loadScene(state, seed, sceneFac, options, matter);
                }
            };
        },
        loadScene: function(state, seed, sceneFac, options, matter) {
            state.scene = sceneFac.create(seed, matter, options);
            return MatterEngine.draw(state);
        },
        draw: function(state) {
            var timeDelta;
            if (!state.scene) {
                return;
            }
            requestAnimationFrame(function() {
                return MatterEngine.draw(state);
            });
            timeDelta = 16;
            Matter.Engine.update(state.matter, timeDelta);
            state.scene.update();
            return state.matter.render.controller.world(state.matter);
        }
    };

    MatterEngine.SceneFactory = {
        create: function(childFactory, matter) {
            var state;
            return state = {
                children: [],
                update: function() {
                    return MatterEngine.SceneFactory.update(state, childFactory);
                },
                addMatter: function(items) {
                    return MatterEngine.SceneFactory.addMatter(state, items);
                },
                addMouseEvents: function(canvasId, width, height, position) {
                    return MatterEngine.SceneFactory.addMouseEvents(state, canvasId, width, height, position);
                },
                matter: matter
            };
        },
        addMouseEvents: function(state, canvasId, width, height, position) {
            state.mouse = position;
            return document.getElementById(canvasId).addEventListener('mousemove', function(event) {
                return MatterEngine.SceneFactory.onMouseMove(state, width, height, event);
            });
        },
        onMouseMove: function(state, width, height, arg) {
            var offsetX, offsetY, target, x, y;
            target = arg.target, offsetX = arg.offsetX, offsetY = arg.offsetY;
            if (target.nodeName !== 'CANVAS') {
                return;
            }
            x = Math.max(offsetX, 0);
            x = Math.min(x, width - 1);
            y = Math.max(offsetY, 0);
            y = Math.min(y, height - 1);
            return state.mouse = {
                x: x,
                y: y
            };
        },
        update: function(state, childFactory) {
            var child, i, len, ref;
            ref = state.children;
            for (i = 0, len = ref.length; i < len; i++) {
                child = ref[i];
                child.update();
            }
            return childFactory.update(state);
        },
        addMatter: function(state, items) {
            var i, item, len, results;
            results = [];
            for (i = 0, len = items.length; i < len; i++) {
                item = items[i];
                if (!item.vertices) {
                    continue;
                }
                item.matterBody = Matter.Bodies.fromVertices(item.x, item.y, item.vertices, item.matterOptions);
                Matter.World.add(state.matter.world, [item.matterBody]);
                results.push(state.children.push(item));
            }
            return results;
        }
    };

    MatterEngine.Utils = {
        rotateVertices: function(vertices, rotationRad) {
            var i, len, results, vertex, x, y;
            results = [];
            for (i = 0, len = vertices.length; i < len; i++) {
                vertex = vertices[i];
                x = vertex.x, y = vertex.y;
                vertex.x = x * Math.cos(rotationRad) - y * Math.sin(rotationRad);
                results.push(vertex.y = x * Math.sin(rotationRad) + y * Math.cos(rotationRad));
            }
            return results;
        },
        createOrb: function(radius, edgeCount) {
            var angleRad, i, index, ref, vertices, x, y;
            if (edgeCount == null) {
                edgeCount = 10;
            }
            vertices = [];
            for (index = i = 0, ref = edgeCount; 0 <= ref ? i < ref : i > ref; index = 0 <= ref ? ++i : --i) {
                angleRad = ((Math.PI * 2) / (edgeCount - 1)) * index;
                x = Math.cos(angleRad) * radius;
                y = Math.sin(angleRad) * radius;
                vertices.push({
                    x: x,
                    y: y
                });
            }
            return vertices;
        }
    };

    window.MatterEngine = MatterEngine;

}).call(this);