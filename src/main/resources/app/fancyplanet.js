'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

var seed = 5625463739;
var gravityForce = 2;
var shipThrust = 0.00005;
var turnTorque = 0.0002;

var random = function () {
    var s = seed;
    return function () {
        return alea(s++)();
    };
}();

setTimeout(function () {
    var canvasId = 'js-canvas-container';
    var width = 410;
    var height = 410;
    var background = 'transparent';
    var engine = MatterEngine.create(canvasId, { width: width, height: height, background: background, wireframes: false });
    engine.loadScene(seed, MainSceneFactory, { canvasId: canvasId, width: width, height: height });
}, 0);

var MainSceneFactory = {
    create: function create(seed, matter, options) {
        var _ref = [0, 0];
        matter.world.gravity.x = _ref[0];
        matter.world.gravity.y = _ref[1];

        var state = MatterEngine.SceneFactory.create(MainSceneFactory, matter);
        state.input = {
            up: false,
            right: false,
            down: false,
            left: false
        };
        state.options = options;
        state.compass = NavFactory.create(options.width, options.height);
        state.trailBook = new TrailBook(64, options.width, options.height);
        state.asteroidLayer = new PIXI.Container();
        state.compassLayer = new PIXI.Container();
        state.compassLayer.addChild(state.compass.graphics);
        matter.render.container.addChild(state.trailBook.sprite);
        matter.render.container.addChild(state.asteroidLayer);
        matter.render.container.addChild(state.compassLayer);
        var centerX = state.options.width / 2;
        var centerY = state.options.height / 2;

        state.planet = MainSceneFactory.createPlanet(state.asteroidLayer, centerX, centerY);
        state.ship = MainSceneFactory.createShip(state.asteroidLayer, state.planet, centerX, centerY);
        MainSceneFactory.createMatter(state);
        setupEventListeners(state.input);
        state.iteration = 0;
        return state;
    },

    update: function update(state) {
        [state.ship].concat(_toConsumableArray(state.asteroids)).forEach(function (_ref2, index) {
            var matterBody = _ref2.matterBody;

            var acceleration = [0, 0];
            vec2.add(acceleration, acceleration, forces.gravitation(matterBody, [state.planet]));

            var _vec2$add = vec2.add([], [matterBody.force.x, matterBody.force.y], acceleration);

            var _vec2$add2 = _slicedToArray(_vec2$add, 2);

            matterBody.force.x = _vec2$add2[0];
            matterBody.force.y = _vec2$add2[1];
            var position = matterBody.position;
        });
        MainSceneFactory.handleKeys(state);
        var _ref3 = [0 - state.ship.matterBody.position.x + state.options.width / 2, 0 - state.ship.matterBody.position.y + state.options.height / 2];
        state.asteroidLayer.position.x = _ref3[0];
        state.asteroidLayer.position.y = _ref3[1];

        state.compass.render(state.asteroidLayer, state.planet, state.asteroids);
        state.trailBook.render(state.iteration, [state.ship], [0 - state.ship.matterBody.position.x + state.options.width / 2, 0 - state.ship.matterBody.position.y + state.options.height / 2]);
        state.iteration++;
    },

    handleKeys: function handleKeys(_ref4) {
        var input = _ref4.input;
        var ship = _ref4.ship;

        Matter.Body.setAngularVelocity(ship.matterBody, ship.matterBody.angularVelocity * 0.8);
        if (input.left) {
            document.getElementById('left').className = 'key left is-down';
            ship.matterBody.torque = -turnTorque;
        }
        if (input.right) {
            document.getElementById('right').className = 'key right is-down';
            ship.matterBody.torque = turnTorque;
        }
        if (input.up) {
            document.getElementById('up').className = 'key up is-down';
            var angle = (ship.matterBody.angle - Math.PI / 2) % (Math.PI * 2);
            var _ship$matterBody = ship.matterBody;
            var force = _ship$matterBody.force;
            var velocity = _ship$matterBody.velocity;

            force.x += Math.cos(angle) * shipThrust;
            force.y += Math.sin(angle) * shipThrust;
        }
        if (input.down) {
            document.getElementById('down').className = 'key down is-down';
        }
    },

    createAsteroids: function createAsteroids(container, planet, centerX, centerY) {
        var colors = [0xff0000, 0xff5500, 0xffff00, 0x00ff00, 0x0000ff, 0x4b0082, 0x8b00ff];
        var stepSize = Math.PI * 2 / colors.length;
        return Array.apply(undefined, _toConsumableArray(Array(colors.length))).map(function (_, index) {
            var xPos = centerX + Math.cos(index * stepSize) * 380;
            var yPos = centerY + Math.sin(index * stepSize) * 380;
            var asteroid = MainSceneFactory.createAsteroid(container, xPos, yPos, colors[index], 24);
            asteroid.matterOptions.force = {};
            asteroid.matterOptions.friction = 0.9;
            asteroid.matterOptions.frictionAir = 0;
            asteroid.matterOptions.resitution = 0.1;
            asteroid.matterOptions.mass = 1;

            var _calculateCircularOrb = calculateCircularOrbitVector(asteroid, planet, 1);

            var _calculateCircularOrb2 = _slicedToArray(_calculateCircularOrb, 2);

            asteroid.matterOptions.force.x = _calculateCircularOrb2[0];
            asteroid.matterOptions.force.y = _calculateCircularOrb2[1];

            return asteroid;
        });
    },

    createShip: function createShip(container, planet, centerX, centerY) {
        var angle = Math.PI * 2 / 7 / 2;
        var xPos = centerX + Math.cos(angle) * 380;
        var yPos = centerY + Math.sin(angle) * 380;
        var ship = ShipFactory.create(xPos, yPos, 10, 0);
        ship.matterOptions.force = {};
        ship.matterOptions.friction = 0;
        ship.matterOptions.frictionAir = 0;
        ship.matterOptions.resitution = 1;
        ship.matterOptions.mass = 1;

        var _calculateCircularOrb3 = calculateCircularOrbitVector(ship, planet, 1);

        var _calculateCircularOrb4 = _slicedToArray(_calculateCircularOrb3, 2);

        ship.matterOptions.force.x = _calculateCircularOrb4[0];
        ship.matterOptions.force.y = _calculateCircularOrb4[1];

        container.addChild(ship.graphics);
        return ship;
    },

    createPlanet: function createPlanet(container, centerX, centerY) {
        var planet = PlanetFactory.create(centerX, centerY, 200, 0);
        container.addChild(planet.container);
        return planet;
    },

    createMatter: function createMatter(state) {
        var centerX = state.options.width / 2;
        var centerY = state.options.height / 2;
        state.asteroids = MainSceneFactory.createAsteroids(state.asteroidLayer, state.planet, centerX, centerY);
        state.addMatter([state.planet, state.ship].concat(_toConsumableArray(state.asteroids)));
    },

    createAsteroid: function createAsteroid(container, x, y, color) {
        var radius = arguments.length <= 4 || arguments[4] === undefined ? 6 : arguments[4];

        var asteroid = AsteroidFactory.create(x, y, radius, 0, color, (Math.floor(random() * 3) + 3) * 2);
        container.addChild(asteroid.graphics);
        return asteroid;
    }
};

function calculateCircularOrbitVector(target, planet) {
    var velocityModifier = arguments.length <= 2 || arguments[2] === undefined ? 1 : arguments[2];

    var angle = Math.atan2(planet.y - target.y, planet.x - target.x);
    var desired = vec2.sub([], [planet.x, planet.y], [target.x, target.y]);
    var distance = vec2.len(desired);
    var gravityStrength = gravityForce / (distance * distance);
    var neighborGravity = vec2.mul([], vec2.normalize([], desired), [gravityStrength, gravityStrength]);
    var velocity = Math.sqrt(distance / vec2.len(neighborGravity)) * vec2.len(neighborGravity) / 16; // refactor, magic number
    velocity *= velocityModifier;
    var output = vec2.mul([], [Math.sin(-angle), Math.cos(angle)], [velocity, velocity]);
    return output;
}

function generatePolygon(avgRadius) {
    var pointCount = arguments.length <= 1 || arguments[1] === undefined ? 6 : arguments[1];
    var varianceFactor = arguments.length <= 2 || arguments[2] === undefined ? 0.5 : arguments[2];

    var stepSize = Math.PI * 2 / pointCount;
    var variance = avgRadius * varianceFactor;
    return Array.apply(undefined, _toConsumableArray(Array(pointCount))).map(function (_, index) {
        return {
            x: Math.floor(Math.cos(stepSize * index) * (avgRadius + (random() * variance - variance / 2))),
            y: Math.floor(Math.sin(stepSize * index) * (avgRadius + (random() * variance - variance / 2)))
        };
    });
}

var forces = {
    gravitation: function gravitation(target, neighbors) {
        var vectorSum = [0, 0];
        var steeringVector = [0, 0];
        var count = 0;
        neighbors.forEach(function (neighbor) {
            var distance = vec2.dist([target.position.x, target.position.y], [neighbor.matterBody.position.x, neighbor.matterBody.position.y]);
            if (distance > 0) {
                vec2.add(vectorSum, vectorSum, [neighbor.matterBody.position.x, neighbor.matterBody.position.y]);
                count++;
            }
        });
        if (count) {
            var baryCenter = vec2.div([], vectorSum, [count, count]);
            var desired = vec2.sub([], baryCenter, [target.position.x, target.position.y]);
            var distance = vec2.len(desired);
            var gravityStrength = gravityForce / (distance * distance);
            var neighborGravity = vec2.mul([], vec2.normalize([], desired), [gravityStrength, gravityStrength]);
            vec2.sub(steeringVector, neighborGravity, [target.force.x, target.force.y]);
        }
        return steeringVector;
    }
};

function setupEventListeners(input) {
    window.addEventListener('keydown', handleKeyDown(input));
    window.addEventListener('keyup', handleKeyUp(input));
}

function handleKeyUp(input) {
    return function (_ref5) {
        var keyCode = _ref5.keyCode;

        document.getElementById('left').className = 'key left';
        document.getElementById('right').className = 'key right';
        document.getElementById('up').className = 'key up';
        document.getElementById('down').className = 'key down';
        switch (keyCode) {
            case 38:
                input.up = false;
                break;
            case 39:
                input.right = false;
                break;
            case 40:
                input.down = false;
                break;
            case 37:
                input.left = false;
                break;
        }
    };
}

function handleKeyDown(input) {
    return function (e) {
        var keyCode = e.keyCode;

        switch (keyCode) {
            case 38:
                input.up = true;
                break;
            case 39:
                input.right = true;
                break;
            case 40:
                e.preventDefault();
                input.down = true;
                break;
            case 37:
                input.left = true;
                break;
        }
    };
}

function hideClickArea() {
    document.getElementById('js-click-area').style.display = 'none';
}

var NavFactory = {
    create: function create(width, height) {
        var graphics = new PIXI.Graphics();
        var state = {
            graphics: graphics,
            width: width,
            height: height,
            x: 0,
            y: 0,
            render: function render(parentContainer, planet, asteroids) {
                return NavFactory.render(state, parentContainer, planet, asteroids);
            }
        };
        return state;
    },

    render: function render(state, parentContainer, planet, asteroids) {
        var centerX = state.width / 2;
        var centerY = state.height / 2;
        var graphics = state.graphics;

        graphics.clear();
        [planet].concat(_toConsumableArray(asteroids)).forEach(function (trackable) {
            var position = trackable.matterBody.position;

            var _vec2$sub = vec2.sub([], [position.x + parentContainer.position.x, position.y + parentContainer.position.y], [centerX, centerY]);

            var _vec2$sub2 = _slicedToArray(_vec2$sub, 2);

            var vX = _vec2$sub2[0];
            var vY = _vec2$sub2[1];

            if (Math.abs(vX) - trackable.size < centerX && Math.abs(vY) - trackable.size < centerY) {
                return;
            }
            var angle = Math.atan2(vY, vX);
            var xPos = centerX + Math.cos(angle) * 300;
            var yPos = centerY + Math.sin(angle) * 300;
            xPos = Math.min(state.width, Math.max(0, xPos));
            yPos = Math.min(state.height, Math.max(0, yPos));
            graphics.beginFill(trackable.color);
            graphics.drawRect(xPos - 5, yPos - 5, 10, 10);
            graphics.endFill();
        });
    }
};

var PlanetFactory = {
    create: function create(x, y, size, rotation) {
        var container = new PIXI.Container();
        var atmosphere = new Atmosphere(-size * 2, -size * 2, size, '#9ad2fc', size * 4, size * 4);
        atmosphere.cache();
        container.addChild(atmosphere.sprite);
        var planet = new Planet(-size * 2, -size * 2, size, ['#1197c0', '#4bdf6b'], random() * Math.PI * 2, size * 4, size * 4); // ocean, land
        planet.cache();
        container.addChild(planet.sprite);
        var state = {
            color: 0xdedede,
            planet: planet,
            atmosphere: atmosphere,
            container: container,
            rotation: rotation,
            size: size,
            x: x,
            y: y,
            update: function update(options) {
                return PlanetFactory.update(state, options);
            },
            vertices: generatePolygon(size, 20, 0),
            matterOptions: {
                friction: 0.9,
                resitution: 0.01,
                isStatic: true,
                mass: 999999
            }
        };
        return state;
    },

    update: function update(state) {
        var container = state.container;
        var matterBody = state.matterBody;
        var vertices = state.vertices;
        var _matterBody$position = matterBody.position;
        var x = _matterBody$position.x;
        var y = _matterBody$position.y;
        var _ref6 = [x, y];
        container.position.x = _ref6[0];
        container.position.y = _ref6[1];

        container.rotation = (container.rotation + 0.001) % (Math.PI * 2);
        container.cacheAsBitmap = true;
    }
};

var AsteroidFactory = {
    create: function create(x, y, size, rotation, color, vertexCount) {
        var varianceFactor = arguments.length <= 6 || arguments[6] === undefined ? 0.5 : arguments[6];

        var graphics = new PIXI.Graphics();
        var state = {
            color: color,
            graphics: graphics,
            rotation: rotation,
            size: size,
            x: x,
            y: y,
            update: function update(options) {
                return AsteroidFactory.update(state, options);
            },
            vertices: generatePolygon(size, vertexCount, varianceFactor),
            matterOptions: {
                friction: 0,
                resitution: 0.01
            }
        };
        return state;
    },

    update: function update(state) {
        var graphics = state.graphics;
        var matterBody = state.matterBody;
        var vertices = matterBody.vertices;
        var _matterBody$position2 = matterBody.position;
        var x = _matterBody$position2.x;
        var y = _matterBody$position2.y;

        Matter.Body.setAngle(matterBody, graphics.rotation);
        Matter.Vertices.rotate(vertices, matterBody.angle, matterBody.position);
        var _ref7 = [x, y];
        graphics.position.x = _ref7[0];
        graphics.position.y = _ref7[1];

        graphics.clear();
        graphics.beginFill(state.color);
        graphics.lineStyle(2, 0xdedede);
        graphics.moveTo(vertices[0].x - x, vertices[0].y - y);
        for (var index = 1; index < vertices.length; index++) {
            graphics.lineTo(vertices[index].x - x, vertices[index].y - y);
        }
        graphics.lineTo(vertices[0].x - x, vertices[0].y - y);
        graphics.endFill();
        graphics.cacheAsBitmap = true;
    }
};

var ShipFactory = {
    create: function create(x, y, size, rotation) {
        var graphics = new PIXI.Graphics();
        var state = {
            graphics: graphics,
            rotation: rotation,
            size: size,
            x: x,
            y: y,
            update: function update(options) {
                return ShipFactory.update(state, options);
            },
            vertices: ShipFactory.generateVertices(size),
            matterOptions: {
                friction: 0.8,
                airFriction: 0,
                resitution: 0.1
            }
        };
        return state;
    },

    generateVertices: function generateVertices(size) {
        var offset = size / 2;
        return [{ x: size / 2 - offset, y: -offset * 1.7 }, { x: size - offset, y: size - offset }, { x: 0 - offset, y: size - offset }];
    },

    update: function update(state) {
        var graphics = state.graphics;
        var matterBody = state.matterBody;
        var vertices = state.vertices;
        var _matterBody$position3 = matterBody.position;
        var x = _matterBody$position3.x;
        var y = _matterBody$position3.y;

        graphics.rotation = matterBody.angle;
        var _ref8 = [x, y];
        graphics.position.x = _ref8[0];
        graphics.position.y = _ref8[1];

        graphics.clear();
        graphics.beginFill(0x333333);
        graphics.lineStyle(2, 0xdedede);
        graphics.moveTo(vertices[0].x, vertices[0].y);
        for (var index = 1; index < vertices.length; index++) {
            graphics.lineTo(vertices[index].x, vertices[index].y);
        }
        graphics.lineTo(vertices[0].x, vertices[0].y);
        graphics.endFill();
        graphics.cacheAsBitmap = true;
    }
};

var emojiLookup = function () {
    var size = 16;
    var surfaceArr = '🌵 🎪 🌴 🏠 🏥 🏣 🏪 🏫 💒 🚉 🚃'.split(' ');
    return surfaceArr.map(function (emoji) {
        var canvasEl = document.createElement('canvas');
        canvasEl.width = size + size / 2;
        canvasEl.height = size + size / 2;
        var ctx = canvasEl.getContext('2d');
        ctx.font = '16px AppleColorEmoji';
        ctx.fillStyle = 'white';
        ctx.textAlign = 'center';
        ctx.textBaseline = 'middle';
        ctx.fillText(emoji, canvasEl.width / 2, canvasEl.height / 2);
        return canvasEl;
    });
}();

var Atmosphere = function () {
    function Atmosphere(x, y, radius, color, width, height) {
        _classCallCheck(this, Atmosphere);

        this.radius = radius;
        this.color = color;
        var size = this.radius * 2 * 2;
        var cacheEl = document.createElement('canvas');
        cacheEl.width = width;
        cacheEl.height = height;
        this.cacheCtx = cacheEl.getContext('2d');
        this.texture = PIXI.Texture.fromCanvas(cacheEl);
        this.sprite = new PIXI.Sprite(this.texture);
        this.sprite.position.x = x;
        this.sprite.position.y = y;
    }

    _createClass(Atmosphere, [{
        key: 'cache',
        value: function cache() {
            var ctx = this.cacheCtx;
            var color = chroma(this.color);
            var atmosphereSize = 2;
            var atmosphereRadius = this.radius * atmosphereSize;
            var halfSize = atmosphereRadius / 2;
            var x = atmosphereRadius;
            var y = atmosphereRadius;
            ctx.beginPath();
            var baseSize = 0.99 / atmosphereSize;
            var gradient = ctx.createRadialGradient(x, y, 0, x, y, atmosphereRadius);
            gradient.addColorStop(baseSize, 'rgba(' + color.alpha(0.4).rgba() + ')');
            gradient.addColorStop(baseSize + baseSize * 0.06, 'rgba(' + color.alpha(0.125).rgba() + ')');
            gradient.addColorStop(baseSize + baseSize * 0.18, 'rgba(' + color.alpha(0.118).rgba() + ')');
            gradient.addColorStop(baseSize + baseSize * 0.23, 'rgba(' + color.alpha(0.118).rgba() + ')');
            gradient.addColorStop(baseSize + baseSize * 0.28, 'rgba(' + color.alpha(0.108).rgba() + ')');
            gradient.addColorStop(baseSize + baseSize * 0.3, 'rgba(' + color.alpha(0.116).rgba() + ')');
            gradient.addColorStop(baseSize + baseSize * 0.5, 'rgba(' + color.alpha(0.07).rgba() + ')');
            gradient.addColorStop(baseSize + baseSize * 0.7, 'rgba(' + color.alpha(0.04).rgba() + ')');
            gradient.addColorStop(1, 'rgba(' + color.alpha(0).rgba() + ')');
            ctx.fillStyle = gradient;
            ctx.arc(x, y, atmosphereRadius, 0, Math.PI * 2); // refactor, bug, this should be 0, 0
            ctx.fill();
        }
    }]);

    return Atmosphere;
}();

var Planet = function () {
    function Planet(x, y, radius, colors, rotation, width, height) {
        _classCallCheck(this, Planet);

        this.x = x;
        this.y = y;
        this.rotation = rotation;
        this.radius = radius;
        this.colors = colors;
        var size = this.radius * 2 * 2;
        var cacheEl = document.createElement('canvas');
        cacheEl.width = width;
        cacheEl.height = height;
        this.cacheCtx = cacheEl.getContext('2d');
        this.texture = PIXI.Texture.fromCanvas(cacheEl);
        this.sprite = new PIXI.Sprite(this.texture);
        this.sprite.position.x = x;
        this.sprite.position.y = y;
    }

    _createClass(Planet, [{
        key: 'generatePlanetData',
        value: function generatePlanetData(lotCount) {
            var data = [];
            for (var lotIndex = 0; lotIndex < lotCount; lotIndex++) {
                data.push(1);
            }
            var backgroundForestCount = Math.floor(random() * 2) + 1;
            while (backgroundForestCount--) {
                var backgroundForestSize = Math.floor(random() * (lotCount / 8)) + 2;
                var backgroundForestIndex = Math.floor(random() * lotCount);
                while (backgroundForestSize--) {
                    data[backgroundForestIndex] = 2;
                    backgroundForestIndex++;
                    backgroundForestIndex = backgroundForestIndex % lotCount;
                }
            }
            var foregroundForestCount = Math.floor(random() * 2) + 1;
            while (foregroundForestCount--) {
                var foregroundForestSize = Math.floor(random() * (lotCount / 8)) + 2;
                var foregroundForestIndex = Math.floor(random() * lotCount);
                while (foregroundForestSize--) {
                    data[foregroundForestIndex] = 3;
                    foregroundForestIndex++;
                    foregroundForestIndex = foregroundForestIndex % lotCount;
                }
            }
            var oceanCount = Math.floor(random() * 2) + 1;
            while (oceanCount--) {
                var oceanSize = Math.floor(random() * (lotCount / 6)) + 3;
                var oceanIndex = Math.floor(random() * lotCount);
                while (oceanSize--) {
                    data[oceanIndex] = 0;
                    oceanIndex++;
                    oceanIndex = oceanIndex % lotCount;
                }
            }
            return data;
        }
    }, {
        key: 'drawOcean',
        value: function drawOcean(ctx, x, y, radius, color, data, mantleSize, oceanSize) {
            var oceanRadius = radius * oceanSize;
            ctx.beginPath();
            var gradient = ctx.createRadialGradient(x, y, 0, x, y, oceanRadius);
            gradient.addColorStop(1 - (oceanSize - mantleSize), chroma(color).darken().hex());
            gradient.addColorStop(1, color);
            ctx.fillStyle = gradient;
            ctx.arc(x, y, oceanRadius, 0, Math.PI * 2);
            ctx.fill();
        }
    }, {
        key: 'drawForests',
        value: function drawForests(ctx, x, y, radius, color, data, coreSize, mantleSize, continentSize, oceanSize) {
            var size = 16;
            var forestRadius = continentSize * radius + size * 1.8;
            var currentAngle = 0;
            var tau = Math.PI * 2;
            var stepAngle = tau / data.length;
            data.forEach(function (lotType, dataIndex) {
                if (!(lotType === 2 || lotType === 3)) {
                    return;
                }
                var emojiEl = emojiLookup[Math.floor(emojiLookup.length * random())];
                var angle = dataIndex * stepAngle + stepAngle / 2;
                var textX = x + Math.cos(angle) * forestRadius;
                var textY = y + Math.sin(angle) * forestRadius;
                var width = emojiEl.width;
                var height = emojiEl.height;

                var textAngle = (angle + Math.PI / 2 + Math.PI * 2) % (Math.PI * 2);
                ctx.translate(textX, textY);
                ctx.rotate(textAngle);
                ctx.drawImage(emojiEl, 0, 0, width, height, -size / 2, -size / 2, width, height);
                ctx.rotate(-textAngle);
                ctx.translate(-textX, -textY);
            });
        }
    }, {
        key: 'drawContinents',
        value: function drawContinents(ctx, x, y, radius, color, data, coreSize, mantleSize, continentSize, oceanSize) {
            var continentRadius = continentSize * radius;
            var currentAngle = 0;
            var tau = Math.PI * 2;
            var stepAngle = tau / data.length;
            var lineWidth = (oceanSize * radius - continentSize * radius) * 2 * 1.25;
            data.forEach(function (lotType, dataIndex) {
                if (lotType === 0) {
                    return;
                }
                ctx.beginPath();
                ctx.lineCap = 'round';
                ctx.strokeStyle = color;
                ctx.lineWidth = lineWidth;
                ctx.arc(x, y, continentRadius, dataIndex * stepAngle, dataIndex * stepAngle + stepAngle);
                ctx.stroke();
            });
        }
    }, {
        key: 'drawCore',
        value: function drawCore(ctx, x, y, radius, color, coreSize) {
            var coreRadius = radius * coreSize;
            ctx.beginPath();
            var gradient = ctx.createRadialGradient(x, y, 0, x, y, coreRadius);
            gradient.addColorStop(0, color);
            gradient.addColorStop(1, chroma(color).darken().hex());
            ctx.fillStyle = gradient;
            ctx.arc(x, y, coreRadius, 0, Math.PI * 2);
            ctx.fill();
        }
    }, {
        key: 'drawMantle',
        value: function drawMantle(ctx, x, y, radius, color, coreSize, mantleSize) {
            var mantleRadius = radius * mantleSize;
            ctx.beginPath();
            var gradient = ctx.createRadialGradient(x, y, 0, x, y, mantleRadius);
            gradient.addColorStop(1 - (mantleSize - coreSize), chroma(color).darken().hex());
            gradient.addColorStop(1, color);
            ctx.fillStyle = gradient;
            ctx.arc(x, y, mantleRadius, 0, Math.PI * 2);
            ctx.fill();
        }
    }, {
        key: 'cache',
        value: function cache() {
            var ctx = this.cacheCtx;
            var lotCount = 30;
            var coreSize = 0.8;
            var mantleSize = coreSize + coreSize * 0.125;
            var oceanSize = mantleSize + mantleSize * 0.075;
            var continentSize = mantleSize;
            var data = this.generatePlanetData(lotCount);

            var _colors = _slicedToArray(this.colors, 2);

            var oceanColor = _colors[0];
            var landColor = _colors[1];

            var coreColor = chroma(landColor).darken(0.75).hex();
            var x = this.radius * continentSize * 2 + this.radius * (1 - coreSize); // refactor, not sure this is correct
            var y = this.radius * continentSize * 2 + this.radius * (1 - coreSize);
            this.drawOcean(ctx, x, y, this.radius, oceanColor, data, mantleSize, oceanSize); // refactor, bug, this should be 0, 0
            this.drawForests(ctx, x, y, this.radius, landColor, data, coreSize, mantleSize, continentSize, oceanSize);
            this.drawContinents(ctx, x, y, this.radius, landColor, data, coreSize, mantleSize, continentSize, oceanSize);
            this.drawMantle(ctx, x, y, this.radius, landColor, coreSize, mantleSize);
            this.drawCore(ctx, x, y, this.radius, coreColor, coreSize);
        }
    }]);

    return Planet;
}();

var TrailBook = function () {
    function TrailBook(layerCount, width, height) {
        _classCallCheck(this, TrailBook);

        this.width = width;
        this.height = height;
        this.layers = Array.apply(undefined, _toConsumableArray(Array(layerCount))).map(function (_, index) {
            var layerEl = document.createElement('canvas'); // it's very slow to `drawImage` when the canvases are different sizes
            // this takes more space but it runs faster by using many off-screen canvases
            var _ref9 = [width, height];
            layerEl.width = _ref9[0];
            layerEl.height = _ref9[1];
            return {
                ctx: layerEl.getContext('2d'),
                canvas: layerEl,
                x: width / 2,
                y: height / 2
            };
        });
        var trailEl = document.createElement('canvas');
        var _ref10 = [width, height];
        trailEl.width = _ref10[0];
        trailEl.height = _ref10[1];

        this.trailCtx = trailEl.getContext('2d');
        this.texture = PIXI.Texture.fromCanvas(trailEl);
        this.sprite = new PIXI.Sprite(this.texture);
    }

    _createClass(TrailBook, [{
        key: 'render',
        value: function render(iteration, trailables, _ref11) {
            var _this = this;

            var _ref12 = _slicedToArray(_ref11, 2);

            var offsetX = _ref12[0];
            var offsetY = _ref12[1];

            var layer = this.layers.pop();
            layer.ctx.clearRect(0, 0, this.width, this.height);
            if (iteration % Math.floor(60 / 3) === 0) {
                (function () {
                    layer.ctx.fillStyle = 'orange';
                    var trailSize = 4;
                    trailables.forEach(function (_ref13) {
                        var graphics = _ref13.graphics;
                        var matterBody = _ref13.matterBody;

                        var angle = Math.atan2(matterBody.velocity.y, matterBody.velocity.x);
                        var xPos = matterBody.position.x + offsetX;
                        var yPos = matterBody.position.y + offsetY;
                        var _ref14 = [matterBody.position.x, matterBody.position.y];
                        layer.x = _ref14[0];
                        layer.y = _ref14[1];

                        layer.ctx.translate(xPos, yPos);
                        layer.ctx.rotate(angle);
                        layer.ctx.fillRect(-trailSize / 2, -trailSize / 2, trailSize, trailSize);
                        layer.ctx.rotate(-angle);
                        layer.ctx.translate(-xPos, -yPos);
                    });
                })();
            }
            this.layers.unshift(layer);
            this.trailCtx.clearRect(0, 0, this.width, this.height);
            var alphaStepSize = 1 / this.layers.length;
            this.layers.forEach(function (layer, index) {
                _this.trailCtx.globalAlpha = (_this.layers.length - 1 - index) * alphaStepSize;
                var xPos = layer.x + offsetX - _this.width / 2;
                var yPos = layer.y + offsetY - _this.height / 2;

                _this.trailCtx.translate(xPos, yPos);
                _this.trailCtx.drawImage(layer.canvas, 0, 0, _this.width, _this.height, 0, 0, _this.width, _this.height);
                _this.trailCtx.translate(-xPos, -yPos);
            });
            this.trailCtx.globalAlpha = 1;
        }
    }]);

    return TrailBook;
}();

// fps meter


(function () {
    var script = document.createElement('script');script.onload = function () {
        var stats = new Stats();document.body.appendChild(stats.dom);requestAnimationFrame(function loop() {
            stats.update();requestAnimationFrame(loop);
        });
    };script.src = '//rawgit.com/mrdoob/stats.js/master/build/stats.min.js';document.head.appendChild(script);
})();
