'use strict';

var core = require('/lib/core');
var router = require('/lib/router')();

require('/controller/assets.js')(router);
require('/controller/fancyplanet.js')(router);

exports.service = function (req) {
    return router.dispatch(req);
};

router.get('/', function (req) {
    return { redirect: '/fancyplanet', status: 303 };
});

// Handle error
exports.handleError = function (error) {
    return {
        body: {
            status: error.status,
            message: error.message
        }
    };
};
